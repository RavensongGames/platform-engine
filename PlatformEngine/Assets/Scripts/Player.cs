﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Controller2D))]
public class Player : MonoBehaviour {

	public Vector2 jumpHeights = new Vector2(0.25f, 3.25f);
	public float timeToJumpApex;
	public float moveSpeed;
	public bool canWallJump = false;

	public Vector2 wallJumpClimb;
	public Vector2 wallJumpOff;
	public Vector2 wallLeap;

	float wallSlideSpeedMax = 3, wallStickTime = 0.35f, timeToWallUnstick;

	float dashTime = 0f, maxDashTime = 0.2f;
	bool canDash = true, doubleJumpAvailable;

	float gravity, velocityXSmoothing, maxJumpVelocity, minJumpVelocity;
	Vector2 velocity;

	Controller2D controller;
	SpriteRenderer sRenderer;

	public GameObject hitbox;
	public GameObject hitbox2;
	public GameObject bullet;
	float moveXPause = 0f;

	void Start() {
		controller = GetComponent<Controller2D>();
		sRenderer = GetComponentInChildren<SpriteRenderer>();

		gravity = -(2 * jumpHeights.y) / Mathf.Pow(timeToJumpApex, 2);
		maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * jumpHeights.x);
	}

	IEnumerator SpawnHitbox(GameObject g) {
		g.SetActive(true);
		Hitbox h = g.GetComponent<Hitbox>();

		float y = 0;
		float rotZ = 0;
		h.forceType = Hitbox.ForceType.FacingDirection;

		if (Input.GetAxisRaw("Vertical") > 0) {
			y = 1;
			h.forceType = Hitbox.ForceType.FacingUp;
			rotZ = 45;
		} else if (Input.GetAxisRaw("Vertical") < 0) {
			y = -1;
			h.forceType = Hitbox.ForceType.Down;
			rotZ = -45;
		}

		g.GetComponent<SpriteRenderer>().flipX = controller.collisions.faceDir < 0;
		g.transform.localRotation = Quaternion.Euler(0, 0, rotZ * controller.collisions.faceDir);
		g.transform.localPosition = new Vector3(1.8f * controller.collisions.faceDir, y);

		yield return new WaitForSeconds(0.1f);
		g.SetActive(false);
	}

	void Update() {
		Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
		int wallDirX = (controller.collisions.left) ? -1 : 1;
		float targetVelocityX = input.x * moveSpeed;
		float groundDampFactor = (controller.collisions.slidingSlope) ? 0.5f : 0.01f;
		float airDampFactor = 0.1f;

		velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? groundDampFactor : airDampFactor);

		bool wallSliding = false;

		if (canWallJump && (controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0) {
			wallSliding = canDash = doubleJumpAvailable = true;

			if (velocity.y < -wallSlideSpeedMax)
				velocity.y = -wallSlideSpeedMax;

			if (timeToWallUnstick > 0) {
				velocityXSmoothing = 0;
				velocity.x = 0;

				if (input.x != wallDirX && input.x != 0) {
					timeToWallUnstick -= Time.deltaTime;
				} else {
					timeToWallUnstick = wallStickTime;
				}
			} else {
				timeToWallUnstick = wallStickTime;
			}
		}

		//If we hit something above or below
		if (controller.collisions.above || controller.collisions.below) {
			if (controller.collisions.slidingSlope) {
				velocity.y += (controller.collisions.slopeNormal.y * -gravity * Time.deltaTime);
			} else {
				velocity.y = 0;

				//Reset our ability to dash and double jump.
				if (controller.collisions.below) {
					canDash = true;
					doubleJumpAvailable = true;
				}
			}
		}
			
		if (Input.GetKeyDown(KeyCode.LeftShift) && canDash) {
			dashTime = maxDashTime;
			canDash = false;
		}

		if (Input.GetKeyDown(KeyCode.Space)) {
			Jump(wallSliding, wallDirX, input, ref velocity);
		}

		if (Input.GetKeyDown(KeyCode.Z)) {
			StartCoroutine("SpawnHitbox", hitbox);
			velocity.y = 0;
			moveXPause = 0.1f;
		}

		if (Input.GetKeyDown(KeyCode.X)) {
			StartCoroutine("SpawnHitbox", hitbox2);
			velocity.y = 0;
			moveXPause = 0.25f;
            if (velocity.y == 0)
			    controller.AddForce(new Force2D(new Vector2(controller.collisions.faceDir * 0.2f, 0f), 0.999f));
		}

		if (Input.GetKeyDown(KeyCode.C)) {
			GameObject g = SimplePool.Spawn(bullet, new Vector3(transform.position.x + controller.collisions.faceDir * 1.2f, transform.position.y, 0), Quaternion.identity);
			g.GetComponent<Rigidbody2D>().AddForce(new Vector2(controller.collisions.faceDir * 1000, 0));
		}

		//If we are not dashing
		if (dashTime <= 0f) {
			if (Input.GetKeyUp(KeyCode.Space) && velocity.y > minJumpVelocity)
				velocity.y = minJumpVelocity;
			
			velocity.y += gravity * Time.deltaTime;
		} else {
			float velX = (velocity.x != 0) ? Mathf.Sign(velocity.x) : velocity.x;
			velocity.x = velX * moveSpeed * 2.5f;
			velocity.y = 0f;
			dashTime -= Time.deltaTime;
		}

		if (input.x != 0)
			sRenderer.flipX = (input.x < 0);

		if (moveXPause > 0f) {
			moveXPause -= Time.deltaTime;
			if (controller.collisions.below)
				velocity.x = 0;
		}

		controller.Move(velocity * Time.deltaTime, input, false);
	}

	void Jump(bool wallSliding, int wallDirX, Vector2 input, ref Vector2 velocity) {
		if (controller.collisions.clinging) {
			velocity.x = 0f;
			velocity.y = wallJumpClimb.y;

		} else if (wallSliding) {
			if (wallDirX == input.x) {
				velocity.x = -wallDirX * wallJumpClimb.x;
				velocity.y = wallJumpClimb.y;
			} else if (input.x == 0) {
				velocity.x = -wallDirX * wallJumpOff.x;
				velocity.y = wallJumpOff.y;
			} else {
				velocity.x = -wallDirX * wallLeap.x;
				velocity.y = wallLeap.y;
			}
		} else {
			if (controller.collisions.below) {
				if (controller.collisions.slidingSlope) {
					if (input.x != -Mathf.Sign(controller.collisions.slopeNormal.x)) {
						velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
						velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
					}
				} else {
					velocity.y = maxJumpVelocity;
				}
			} else if (doubleJumpAvailable) {
				doubleJumpAvailable = false;
				if (controller.collisions.slidingSlope) {
					if (input.x != -Mathf.Sign(controller.collisions.slopeNormal.x)) {
						velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
						velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
					}
				} else {
					velocity.y = maxJumpVelocity;
				}
			}
		}
	}
}