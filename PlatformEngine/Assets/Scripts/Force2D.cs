﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Force2D {
	public Vector2 velocity;
	public float falloff;

	public Force2D(Vector2 vel, float fall) {
		velocity = vel;
		falloff = fall;
	}

	public void DecreaseForce(float weight) {
		weight = Mathf.Clamp(weight, 0.01f, 0.99f);
		velocity *= (falloff * weight);
	}
}
