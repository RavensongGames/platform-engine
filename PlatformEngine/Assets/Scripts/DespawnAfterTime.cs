﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnAfterTime : MonoBehaviour {

	public float timeToDestroy;

	void OnEnable () {
		Invoke("Despawn", timeToDestroy);
	}

	void Despawn() {
		SimplePool.Despawn(this.gameObject);
	}
}
