﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour {

	Controller2D parentController;
	public float forceMultiplier = 0.01f;
	public float decay = 0.8f;
	public ForceType forceType;
	public float stunTime = 0.1f;
	public bool destroyOnCollision = false;
	public GameObject objectToSpawn;

	void OnTriggerEnter2D(Collider2D col) {
		if (parentController == null)
			parentController = transform.root.GetComponent<Controller2D>();

		if (col.transform.tag == "Player")
			return;

		if (col.transform.GetComponent<Controller2D>() && col.transform != transform.root) {
			Controller2D cont = col.transform.GetComponent<Controller2D>();

			Vector2 diff = Vector2.zero;

			if (forceType == ForceType.FacingDirection && parentController != null) {
				diff = new Vector2(parentController.collisions.faceDir, 0);
			} else if (forceType == ForceType.Up) {
				diff = new Vector2(0.0f, 1.0f);
			} else if (forceType == ForceType.Down) {
				diff = new Vector2(0.0f, -1.0f);
			} else if (forceType == ForceType.CenterOut) {
				diff = new Vector2(col.transform.position.x - transform.position.x, col.transform.position.y - transform.position.y);
			} else if (forceType == ForceType.FacingUp) {
				diff.y = 1;
			} else if (forceType == ForceType.FacingDown) {
				diff.y = -1;
			} else if (forceType == ForceType.CenterOutX) {
				diff = new Vector2(col.transform.position.x - transform.position.x, 0);
			} else if (forceType == ForceType.CenterOutY) {
				diff = new Vector2(0, col.transform.position.y - transform.position.y);
			}

			diff.Normalize();
			cont.AddForce(new Force2D(diff * forceMultiplier, decay));
			cont.GetComponent<NPC>().Stun(stunTime);
		}

		if (objectToSpawn != null)
			SimplePool.Spawn(objectToSpawn, transform.position + Vector3.back, Quaternion.Euler(0, 0, Random.Range(0, 360)));

		if (destroyOnCollision) {
			SimplePool.Despawn(gameObject);
		}
	}

	public enum ForceType {
		FacingDirection, Up, Down, CenterOut, FacingUp, FacingDown, CenterOutX, CenterOutY
	}
}
