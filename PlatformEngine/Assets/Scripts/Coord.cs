﻿using UnityEngine;

[System.Serializable]
public class Coord  {
	[SerializeField] private int _x;
	[SerializeField] private int _y;

	public Coord() {
		_x = 0;
		_y = 0;
	}

	public Coord(int mx, int my) {
		_x = mx;
		_y = my;
	}

	public Coord(Coord other) {
		_x = other.x;
		_y = other.y;
	}

	public int x {
		get { return _x; }
		set { _x = value; }
	}

	public int y {
		get { return _y; }
		set { _y = value; }
	}

	public bool Equals(Coord other) {
		if (ReferenceEquals(null, other)) 
			return false;
		if (ReferenceEquals(this, other)) 
			return true;
		
		return other.x == x && other.y == y;
	}

	public override bool Equals(object obj) {
		if (ReferenceEquals(null, obj)) 
			return false;
		if (ReferenceEquals(this, obj)) 
			return true;
		
		return obj.GetType() == typeof(Coord) && Equals((Coord)obj);
	}


	public bool directionIsDiagonal() {
		if(Mathf.Abs(x) + Mathf.Abs(y) > 1) return true;
		return false;
	}

	public Vector2 toVector2() { 
		return new Vector2(x, y); 
	}
	public override string ToString() { 
		return string.Format("({0}, {1})", x, y); 
	}
	public override int GetHashCode() { 
		unchecked { return (x * 397) ^ y; } 
	}
	public static bool operator == (Coord left, Coord right) { 
		return Equals(left, right); 
	}
	public static bool operator != (Coord left, Coord right) { 
		return !Equals(left, right); 
	}
	public static Coord operator + (Coord left, Coord right) { 
		return new Coord(left.x + right.x, left.y + right.y); 
	}
	public static Coord operator - (Coord left, Coord right) { 
		return new Coord(left.x - right.x, left.y - right.y); 
	}
}
