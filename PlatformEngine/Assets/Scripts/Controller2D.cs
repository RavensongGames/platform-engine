﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Controller2D : RaycastController {

	public CollisionInfo collisions;
	public float weight;
	[HideInInspector] public Vector2 input;

	float maxSlopeAngle = 60;
	List<Force2D> forces;

	public override void Start() {
		base.Start();
		collisions.faceDir = 1;
		forces = new List<Force2D>();
	}
		
	public void Move(Vector2 velocity, bool onPlatform) {
		Move(velocity, Vector2.zero, onPlatform);
	}
		
	public void Move(Vector2 moveAmount, Vector2 _input, bool onPlatform = false) {		
		UpdateRaycastOrigins();
		collisions.Reset();
		input = _input;

		HandleForces(ref moveAmount);

		if (moveAmount.y < 0)
			DescendSlope(ref moveAmount);
		if (moveAmount.x != 0)
			collisions.faceDir = (int)Mathf.Sign(moveAmount.x);

		HorizontalCollisions(ref moveAmount);

		if (moveAmount.y != 0)
			VerticalCollisions(ref moveAmount);

		if (!float.IsNaN(transform.position.x) && !float.IsNaN(transform.position.y) && !float.IsNaN(moveAmount.x) && !float.IsNaN(moveAmount.y)) {
			transform.Translate(moveAmount);
		}

		if (onPlatform)
			collisions.below = true;
	}

	public void AddForce(Force2D f) {
		forces.Add(f);
	}

	void HandleForces(ref Vector2 moveAmount) {
		List<Force2D> tempForces = new List<Force2D>(forces);

		foreach (Force2D f in tempForces) {
			moveAmount += f.velocity;
			f.DecreaseForce(weight);

			if (f.velocity.magnitude < 0.01f)
				forces.Remove(f);
		}
	}
		
	void HorizontalCollisions(ref Vector2 moveAmount) {
		float directionX = collisions.faceDir;
		float rayLength = Mathf.Abs(moveAmount.x) + skinWidth;

		if (Mathf.Abs(moveAmount.x) < skinWidth)
			rayLength = 2 * skinWidth;


		for (int i = 0; i < horizontalRayCount; i++) {			
			Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
			rayOrigin += Vector2.up * (horizontalRaySpacing * i);
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX,rayLength, collisionMask);

			if (hit) {
				foreach (Force2D f in forces) {
					if (f.velocity.x != 0) {
						f.velocity.x = 0;
					}
				}

				if (hit.distance == 0)
					continue;

				float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

				if (i == 0 && slopeAngle <= maxSlopeAngle) {
					float distToSlope = 0f;

					if (slopeAngle != collisions.slopeAngleOld) {
						distToSlope = hit.distance - skinWidth;
						moveAmount.x -= distToSlope * directionX;
					}
						
					ClimbSlope(ref moveAmount, slopeAngle, hit.normal);
					moveAmount.x += distToSlope * directionX;
				}
					
				if (!collisions.climbingSlope || slopeAngle > maxSlopeAngle) {
					moveAmount.x = (hit.distance - skinWidth) * directionX;
					rayLength = hit.distance;

					if (collisions.climbingSlope)
						moveAmount.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x);

					collisions.left = (directionX == -1);
					collisions.right = (directionX == 1);
				}

				if (slopeAngle == 90) {
					if (i == horizontalRayCount - 3) //Penultimate Ray
						collisions.clinging = true;
					else if (i == horizontalRayCount - 2) //Last Ray
						collisions.clinging = false;
				}
			}
		}
			
		if (collisions.clinging && moveAmount.y < 0)
			moveAmount.y = 0;
	}

	//Handle collisions on Y axis.
	void VerticalCollisions(ref Vector2 moveAmount) {
		float directionY = Mathf.Sign(moveAmount.y);
		float rayLength = Mathf.Abs(moveAmount.y) + skinWidth;

		for (int i = 0; i < verticalRayCount; i++) {
			Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
			rayOrigin += Vector2.right * (verticalRaySpacing * i + moveAmount.x);
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

			if (hit) {
				foreach (Force2D f in forces) {
					if (f.velocity.y != 0)
						f.velocity.y = 0;
				}

				if (hit.transform.tag == "Through") {
					if (directionY == 1 || hit.distance == 0 || input.y == -1)
						continue;
				}

				//Snap to the position and don't overshoot.
				moveAmount.y = (hit.distance - skinWidth) * directionY;
				rayLength = hit.distance;

				if (collisions.climbingSlope)
					moveAmount.x = moveAmount.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveAmount.x);

				collisions.below = (directionY == -1);
				collisions.above = (directionY == 1);
			}
		}

		//If we are climbing a slope..
		if (collisions.climbingSlope) {
			float directionX = Mathf.Sign(moveAmount.x);

			rayLength = Mathf.Abs(moveAmount.x + skinWidth);

			//Get the correct origin for the raycast (bottom left or right).
			Vector2 rayOrigin = ((directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight) + Vector2.up * moveAmount.y;
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

			//If we hit something...
			if (hit) {
				float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

				if (slopeAngle != collisions.slopeAngle) {
					moveAmount.x = hit.distance - skinWidth * directionX;

					collisions.slopeAngle = slopeAngle;
					collisions.slopeNormal = hit.normal;
				}
			}
		}
	}

	//Ascending slopes
	void ClimbSlope(ref Vector2 moveAmount, float slopeAngle, Vector2 slopeNormal) {
		float moveDistance = Mathf.Abs(moveAmount.x);
		float climbVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

		//If not jumping or otherwise ascending on a slope...
		if (moveAmount.y <= climbVelocityY) {
			moveAmount.y = climbVelocityY;
			moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);

			collisions.below = true;
			collisions.climbingSlope = true;
			collisions.slopeAngle = slopeAngle;
			collisions.slopeNormal = slopeNormal;
		}
	}
		
	void DescendSlope(ref Vector2 moveAmount) {
		RaycastHit2D maxSlopeHitLeft = Physics2D.Raycast(raycastOrigins.bottomLeft, Vector2.down, Mathf.Abs(moveAmount.y) + skinWidth, collisionMask);
		RaycastHit2D maxSlopeHitRight = Physics2D.Raycast(raycastOrigins.bottomRight, Vector2.down, Mathf.Abs(moveAmount.y) + skinWidth, collisionMask);

		//"^" is the *EXCLUSIVE OR* operator. So one or the other, not both. We do this to make sure we are on an angle facing one way or another.
		if (maxSlopeHitLeft ^ maxSlopeHitRight) {
			//Calculate our sliding velocity for either scenario. We pass in the RaycastHit2Ds and a reference to the move amount.
			SlideDownSlope(maxSlopeHitLeft, ref moveAmount); //Left slide
			SlideDownSlope(maxSlopeHitRight, ref moveAmount); //Right slide
		}

		if (!collisions.slidingSlope) {
			float directionX = Mathf.Sign(moveAmount.x);
			Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomRight : raycastOrigins.bottomLeft;
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, -Vector2.up, Mathf.Infinity, collisionMask);

			if (hit) {
				float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

				if (slopeAngle != 0 && slopeAngle <= maxSlopeAngle) {
					if (Mathf.Sign(hit.normal.x) == directionX) {
						
						if (hit.distance - skinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x)) {
							float moveDistance = Mathf.Abs(moveAmount.x);

							//Math-y stuff to calculate the amount to move on both axes.
							moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
							moveAmount.y -= Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

							//Set our collision struct's values.
							collisions.slopeAngle = slopeAngle;
							collisions.slopeNormal = hit.normal;
							collisions.descendingSlope = true;

							//Make sure to show we are definitely grounded.
							collisions.below = true;
						}
					}
				}
			}
		}
	}
		
	void SlideDownSlope(RaycastHit2D hit, ref Vector2 moveAmount) {
		if (hit) {
			float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

			if (slopeAngle > maxSlopeAngle || input.y < 0 && slopeAngle != 0) {
				moveAmount.x = Mathf.Sign(hit.normal.x) * ((Mathf.Abs(moveAmount.y) - hit.distance) / Mathf.Tan(slopeAngle * Mathf.Deg2Rad)) / 2f;

				//Make sure to set the collision struct's values.
				collisions.slopeAngle = slopeAngle;
				collisions.slidingSlope = true;
				collisions.slopeNormal = hit.normal;
				collisions.below = true;
				collisions.faceDir = (int)Mathf.Sign(hit.normal.x);
			}
		}
	}
}