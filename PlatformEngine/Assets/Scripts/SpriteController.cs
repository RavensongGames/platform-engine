﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//DO NOT learn from this. This is awful practice. Use an Animator instead.
public class SpriteController : MonoBehaviour {

	public SpriteRenderer spriteRenderer;
	public Sprite idleSprite;
	public Sprite jumpSprite;
	public Sprite fallSprite;
	public Sprite dashSprite;

	public void DisplaySprite(float x, float y, bool grounded, float moveY, bool dashing) {
		if (x != 0)
			spriteRenderer.flipX = x < 0;

		if (grounded)
			spriteRenderer.sprite = idleSprite;
		else
			spriteRenderer.sprite = (moveY > 0) ? jumpSprite : fallSprite;

		if (dashing) {
			spriteRenderer.sprite = dashSprite;
		} else {
			if (y < 0)
				spriteRenderer.sprite = fallSprite;
			else if (y > 0)
				spriteRenderer.sprite = jumpSprite;
		}
	}
}
