﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;
using LitJson;

public class MapCreatorTool : MonoBehaviour {

	public int CurrentTile = 32;
	public GameObject tilePrefab;
	public Transform tileAchor;
	public Sprite[] sprites;
	public MC_Selection_Type selectType = MC_Selection_Type.Paint;

	List<ChangeHolder> changes;
	MapCreator_Cell[,] cells;
	Coord lastSelected = new Coord(0, 0);
	Coord currentMouseOver = new Coord(0, 0);
	Coord mapSize;

	void Start () {
		mapSize = new Coord(10, 10);
		Init();
	}

	void Init() {
		changes = new List<ChangeHolder>();
		cells = new MapCreator_Cell[mapSize.x, mapSize.y];
		PlaceTiles();
	}

	void PlaceTiles() {
		for (int x = 0; x < mapSize.x; x++) {
			for (int y = 0; y < mapSize.y; y++) {
				MapCreator_Cell m = Instantiate(tilePrefab, tileAchor).GetComponent<MapCreator_Cell>();
				m.pos = new Coord(x, y);
				m.SetTile(sprites[0], 0);
				cells[x, y] = m;
			}
		}
	}

	public void Undo() {
		if (changes.Count <= 0)
			return;

		ChangeHolder holder = changes[changes.Count - 1];

		foreach (Change c in holder.changes) {
			cells[c.pos.x, c.pos.y].SetTile(sprites[c.previousID], c.previousID);
		}

		if (holder.previousPos != null)
			lastSelected = holder.previousPos;

		changes.RemoveAt(changes.Count - 1);
	}

	public void FloodFill(MapCreator_Cell start, int tileToReplace) {
		if (start.tileID == CurrentTile)
			return;

		Stack<MapCreator_Cell> stack = new Stack<MapCreator_Cell>();
		ChangeHolder holder = new ChangeHolder();
		stack.Push(start);

		while (stack.Count > 0) {
			MapCreator_Cell cell = stack.Pop();
			if (cell.tileID == tileToReplace) {
				holder.changes.Add(new Change(cell.pos, cell.tileID));
				cell.SetTile(sprites[CurrentTile], CurrentTile);

				if (cell.pos.x < mapSize.x - 1)
					stack.Push(cells[cell.pos.x + 1, cell.pos.y]);
				if (cell.pos.x > 0)
					stack.Push(cells[cell.pos.x - 1, cell.pos.y]);
				if (cell.pos.y < mapSize.y - 1)
					stack.Push(cells[cell.pos.x, cell.pos.y + 1]);
				if (cell.pos.y > 0)
					stack.Push(cells[cell.pos.x, cell.pos.y - 1]);
			}
		}

		changes.Add(holder);
	}

	public void MouseOver(Coord c) {
		currentMouseOver = c;

		if (Input.GetKey(KeyCode.LeftShift)) {
			int x0 = (lastSelected.x >= currentMouseOver.x) ? currentMouseOver.x : lastSelected.x;
			int y0 = (lastSelected.y >= currentMouseOver.y) ? currentMouseOver.y : lastSelected.y;

			int x1 = (lastSelected.x >= currentMouseOver.x) ? lastSelected.x : currentMouseOver.x;
			int y1 = (lastSelected.y >= currentMouseOver.y) ? lastSelected.y : currentMouseOver.y;

			for (int x = 0; x < mapSize.x; x++) {
				for (int y = 0; y < mapSize.y; y++) {
					cells[x, y].image.color = Color.white;
				}
			}

			for (int x = x0; x <= x1; x++) {
				for (int y = y0; y <= y1; y++) {
					cells[x, y].image.color = Color.yellow;
				}
			}
		}
	}

	public void FillAllWithCurrent() {
		for (int x = 0; x < mapSize.x; x++) {
			for (int y = 0; y < mapSize.y; y++) {
				cells[x, y].OnPress(true);
			}
		}
	}

	public void Changed(int x, int y) {
		lastSelected = new Coord(x, y);

		ChangeHolder holder = new ChangeHolder(new List<Change>(), lastSelected);
		holder.changes.Add(new Change(new Coord(x, y), cells[x, y].tileID));
		changes.Add(holder);

		cells[x, y].SetTile(GetSprite(), GetID());
	}

	public void Box(Coord endPos) {
		ChangeHolder holder = new ChangeHolder(new List<Change>(), lastSelected);
		int x0 = (lastSelected.x >= endPos.x) ? endPos.x : lastSelected.x;
		int y0 = (lastSelected.y >= endPos.y) ? endPos.y : lastSelected.y;

		int x1 = (lastSelected.x >= endPos.x) ? lastSelected.x : endPos.x;
		int y1 = (lastSelected.y >= endPos.y) ? lastSelected.y : endPos.y;

		for (int x = x0; x <= x1; x++) {
			for (int y = y0; y <= y1; y++) {
				holder.changes.Add(new Change(new Coord(x, y), cells[x, y].tileID));
				cells[x, y].SetTile(sprites[CurrentTile], CurrentTile);
			}
		}

		changes.Add(holder);
		lastSelected = endPos;
	}

	public Sprite GetSprite() {
		return sprites[CurrentTile];
	}

	public int GetID() {
		return CurrentTile;
	}

	public void FillTool() {
		selectType = MC_Selection_Type.Fill;
	}

	public void PaintTool() {
		selectType = MC_Selection_Type.Paint;
	}

	public void SetCurrentTileID(int i) {
		CurrentTile = i;
	}

	void GetDataFromDirectory(string[] ss) {
		foreach (string s in ss) {
			
		}
	}

	public void NewMap() {
		if (changes != null)
			changes.Clear();

		CurrentTile = 32;
		FillAllWithCurrent();

		if (changes != null)
			changes.Clear();
	}

	void Update() {
		HandleInput();
	}

	void HandleInput() {
		if (Input.GetKeyDown(KeyCode.Z)) {
			Undo();
		}

		if (Input.GetKeyUp(KeyCode.LeftShift)) {
			for (int x = 0; x < mapSize.x; x++) {
				for (int y = 0; y < mapSize.y; y++) {
					cells[x, y].image.color = Color.white;
				}
			}

			cells[currentMouseOver.x, currentMouseOver.y].image.color = Color.magenta;
		}
	}

	public enum MC_Selection_Type {
		Paint, Fill, Place_Object
	}

	public class MapCreator_Screen {
		public string Name;
		public int[] IDs;
		public int elev;

		public MapCreator_Screen(string name, int e, Coord size) {
			Name = name;
			IDs = new int[size.x * size.y];
			elev = e;
		}
	}

	class ChangeHolder {
		public List<Change> changes;
		public Coord previousPos;

		public ChangeHolder() {
			changes = new List<Change>();
		}

		public ChangeHolder(List<Change> c, Coord pp) {
			changes = c;
			previousPos = pp;
		}
	}

	class Change {
		public Coord pos;
		public int previousID;

		public Change(Coord p, int pID) {
			pos = p;
			previousID = pID;
		}
	}
}
