﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class MapCreator_Cell : MonoBehaviour,  IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler {

	MapCreatorTool mct;
	public Image image;
	public int tileID;
	public Coord pos;

	void Start() {
		Initialize();
	}

	void Initialize() {
		if (image == null)
			image = GetComponentInChildren<Image>();
		if (mct == null)
			mct = GameObject.FindObjectOfType<MapCreatorTool>();
	}

	public void SetTile(Sprite s, int id) {
		Initialize();

		image.sprite = s;
		tileID = id;
	}

	Color colorToShow {
		get {
			return (Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.LeftControl)) ? Color.yellow : Color.magenta;
		}
	}

	public void OnPress(bool overrrideMenu = false) {
		Initialize();

		if (mct.selectType == MapCreatorTool.MC_Selection_Type.Paint) {
			if (Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.LeftControl))
				mct.SetCurrentTileID(tileID);
			else if (Input.GetKey(KeyCode.LeftShift))
				mct.Box(pos);
			else
				mct.Changed(pos.x, pos.y);

		} else if (mct.selectType == MapCreatorTool.MC_Selection_Type.Fill) {
			if (Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.LeftControl))
				mct.SetCurrentTileID(tileID);
			else
				mct.FloodFill(this, tileID);

		}
	}

	public void OnPointerEnter(PointerEventData eventData) {
		mct.MouseOver(pos);
		image.color = colorToShow;
		if (Input.GetMouseButton(0))
			OnPress();
	}

	public void OnPointerExit(PointerEventData eventData) {
		image.color = Color.white;
	}

	public void OnPointerDown(PointerEventData eventData) {
		if (eventData.button == PointerEventData.InputButton.Left)
			OnPress();
	}
}
