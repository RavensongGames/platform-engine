﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Room {
	
	public int width, height;

	public int centerX {
		get { return width / 2; }
	}

	public int centerY {
		get { return height / 2; }
	}

	public Coord center {
		get { return new Coord(centerX, centerY); }
	}

	public Room() {}

	public Room(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public Room(Coord c) {
		this.width = c.x;
		this.height = c.y;
	}
}
