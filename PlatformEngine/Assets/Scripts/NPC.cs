﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Controller2D))]
public class NPC : MonoBehaviour {

	public Vector2 jumpHeights = new Vector2(0.25f, 3.25f);
	public float timeToJumpApex;
	public float moveSpeed;
	public bool act;

	float gravity, velocityXSmoothing, maxJumpVelocity;
	Vector2 velocity;

	Controller2D controller;
	Vector2 input;

	Transform player;

	float stunAmount;

	void Start() {
		controller = GetComponent<Controller2D>();
		player = GameObject.FindObjectOfType<Player>().transform;

		gravity = -(2 * jumpHeights.y) / Mathf.Pow(timeToJumpApex, 2);
		maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
	}

	void Update() {
		if (act) {
			if (Vector2.Distance(new Vector2(transform.position.x, 0), new Vector2(player.position.x, 0)) > 3) {
				if (player.position.x > transform.position.x + 1) {
					input.x = 1;
				} else if (player.position.x < transform.position.x - 1){
					input.x = -1;
				}
			} else {
				input.x = 0;
			}
		}


		float targetVelocityX = input.x * moveSpeed;
		float groundDampFactor = (controller.collisions.slidingSlope) ? 0.5f : 0.01f;
		float airDampFactor = 0.1f;

		velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? groundDampFactor : airDampFactor);

		if (controller.collisions.above || controller.collisions.below) {
			if (controller.collisions.slidingSlope) {
				velocity.y += (controller.collisions.slopeNormal.y * -gravity * Time.deltaTime);
			} else {
				velocity.y = 0;
			}
		}

		if (act && stunAmount <= 0 && player.position.y > transform.position.y && Vector2.Distance(transform.position, player.position) < 10) {
			Jump(input, ref velocity);
		}

		if (stunAmount > 0)
			stunAmount -= Time.deltaTime;

		velocity.y += gravity * Time.deltaTime;

		if (stunAmount > 0) {
			velocity = Vector2.zero;
		}

		controller.Move(velocity * Time.deltaTime, false);
	}

	public void Stun(float amount) {
		stunAmount = amount;
	}

	void Jump(Vector2 input, ref Vector2 velocity) {
		if (controller.collisions.below) {
			if (controller.collisions.slidingSlope) {
				if (input.x != -Mathf.Sign(controller.collisions.slopeNormal.x)) {
					velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
					velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
				}
			} else {
				velocity.y = maxJumpVelocity;
			}
		}
	}
}