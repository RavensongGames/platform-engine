﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (BoxCollider2D))]
public class RaycastController : MonoBehaviour {
	 
	internal const float skinWidth = 0.015f;
	public LayerMask collisionMask;
	const float rayDistance = 0.25f;

	[HideInInspector] public int horizontalRayCount, verticalRayCount;
	internal float horizontalRaySpacing;
	internal float verticalRaySpacing;

	internal RaycastOrigins raycastOrigins;
	internal BoxCollider2D coll;

	public virtual void Start () {
		coll = GetComponent<BoxCollider2D>();
		CalculateRaySpacing();
	}
		
	public void CalculateRaySpacing() {
		Bounds bounds = coll.bounds;
		bounds.Expand(skinWidth * -2);

		horizontalRayCount = Mathf.RoundToInt(bounds.size.y / rayDistance);
		verticalRayCount = Mathf.RoundToInt(bounds.size.x / rayDistance);

		horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2, int.MaxValue);
		verticalRayCount = Mathf.Clamp(verticalRayCount, 2, int.MaxValue);

		horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
		verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
	}
		
	public void UpdateRaycastOrigins(){
		Bounds bounds = coll.bounds;
		bounds.Expand(skinWidth * -2);

		raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
		raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
		raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
		raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);

	}
		
	public struct CollisionInfo {
		public bool above, below;
		public bool left, right;

		public int faceDir;

		public bool climbingSlope;
		public bool descendingSlope;
		public bool slidingSlope;
		public bool clinging;
		public float slopeAngle, slopeAngleOld;
		public Vector2 slopeNormal;

		//Resets all the values at the start of a new frame.
		public void Reset() {
			above = below = false;
			left = right = false;
			climbingSlope = false;
			descendingSlope = false;
			slidingSlope = false;
			clinging = false;

			slopeAngleOld = slopeAngle;
			slopeNormal = Vector2.zero;
			slopeAngle = 0f;
		}
	}

	//Where the origins for each directional ray will start.
	public struct RaycastOrigins {
		public Vector2 topLeft, topRight;
		public Vector2 bottomLeft, bottomRight;
	}
}
